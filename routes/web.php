<?php

use App\Http\Controllers\ProfileController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return Inertia::render('Index', [
        'canLogin' => Route::has('login'),
        'canRegister' => Route::has('register'),
        'laravelVersion' => Application::VERSION,
        'phpVersion' => PHP_VERSION,
    ]);
})->name('index');

Route::get('/dashboard', function () {
    return Inertia::render('Dashboard');
})->middleware(['auth', 'verified'])->name('dashboard');

Route::middleware('auth')->group(function () {
    Route::get('/profile', [ProfileController::class, 'edit'])->name('profile.edit');
    Route::patch('/profile', [ProfileController::class, 'update'])->name('profile.update');
    Route::delete('/profile', [ProfileController::class, 'destroy'])->name('profile.destroy');
});

Route::get('posts', [\App\Http\Controllers\PostController::class , 'index'])->name('post.index');
Route::get('posts/create', [\App\Http\Controllers\PostController::class , 'create'])->name('post.create');
Route::get('posts/{post}', [\App\Http\Controllers\PostController::class , 'edit'])->name('post.edit');
Route::post('posts', [\App\Http\Controllers\PostController::class , 'store'])->name('post.store');
Route::patch('posts/{post}', [\App\Http\Controllers\PostController::class , 'update'])->name('post.update');
Route::delete('posts/{post}', [\App\Http\Controllers\PostController::class , 'destroy'])->name('post.destroy');

Route::get('salons', [\App\Http\Controllers\SalonController::class , 'index'])->name('salon.index');
Route::get('salons/create', [\App\Http\Controllers\SalonController::class , 'create'])->name('salon.create');
Route::get('salons/{salon}', [\App\Http\Controllers\SalonController::class , 'edit'])->name('salon.edit');
Route::post('salons', [\App\Http\Controllers\SalonController::class , 'store'])->name('salon.store');
Route::patch('salons/{salon}', [\App\Http\Controllers\SalonController::class , 'update'])->name('salon.update');
Route::delete('salons/{salon}', [\App\Http\Controllers\SalonController::class , 'destroy'])->name('salon.destroy');

Route::get('services', [\App\Http\Controllers\ServiceController::class , 'index'])->name('service.index');

require __DIR__.'/auth.php';
