<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NonWorkingDay extends Model
{
    use HasFactory;
    protected $guarded = false;
    protected $table = 'non_working_days';

    public function service()
    {
        return $this->belongsTo(Service::class);
    }
}
