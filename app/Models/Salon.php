<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Salon extends Model
{
    use HasFactory;
    protected $guarded = false;
    protected $table = 'salons';

    public function services()
    {
        return $this->hasMany(Service::class);
    }



}
