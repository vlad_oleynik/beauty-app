<?php

namespace App\Http\Controllers;

use App\Http\Requests\Salon\StoreRequest;
use App\Http\Requests\Salon\UpdateRequest;
use App\Http\Resources\Salon\SalonResource;
use App\Models\Post;
use App\Models\Salon;

class SalonController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $salons = Salon::all();
        $salons = SalonResource::collection($salons)->resolve();
        return inertia('Salon/Index' , compact('salons'));

    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return inertia('Salon/Create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request)
    {
        Salon::create($request->validated());
        return redirect()->route('salon.index');
    }

    /**
     * Display the specified resource.
     */
    public function show(Post $post)
    {
        return inertia('Post/Show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Salon $salon)
    {
        return inertia('Salon/Edit', compact('salon'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, Salon $salon)
    {
        $salon->update($request->validated());
        return redirect()->route('salon.index');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Salon $salon)
    {
        $salon->delete();
        return redirect()->route('salon.index');
    }
}
