<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Salon;
use App\Models\Service;
use App\Models\User;
use App\Models\WorkHour;
use Database\Factories\WorkHourFactory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
         User::factory()->create([
             'name' => 'admin',
             'role' => 'admin',
             'email' => 'admin@example.com',
         ]);

        User::factory(10)->create();
        Salon::factory(10)->create();
        Service::factory(10)->create();
        WorkHour::factory(10)->create();
    }
}
