<?php

namespace Database\Factories;

use App\Models\Service;
use App\Models\WorkHour;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\WorkHour>
 */
class WorkHourFactory extends Factory
{
    protected $model = WorkHour::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $service = Service::inRandomOrder()->first();
        $dayOfWeek = $this->faker->numberBetween(1, 7);
        $startTime = Carbon::createFromTime($this->faker->numberBetween(0, 23), $this->faker->randomElement([0, 30]));
        $endTime = $startTime->copy()->addMinutes($this->faker->numberBetween(30, 240));

        return [
            'service_id' => $service->id, // Связь с услугой
            'day_of_week' => $dayOfWeek,
            'start_time' => $startTime->format('H:i'),
            'end_time' => $endTime->format('H:i'),
        ];
    }
}
