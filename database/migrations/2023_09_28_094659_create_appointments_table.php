<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->id();
            $table->dateTime('appointment_date_time');
            $table->unsignedBigInteger('service_id');
            $table->unsignedBigInteger('user_id');

            $table->foreign('service_id')
                ->references('id')
                ->on('services');
            $table->foreign('user_id')
                ->references('id')
                ->on('users');
            $table->timestamps();

            $table->index(['service_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('appointments');
    }
};
